

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoOfflineTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet UILabel *videofilePath;
@end

NS_ASSUME_NONNULL_END
