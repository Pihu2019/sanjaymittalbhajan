

#import "VideoPlayOfflineVC.h"
#import "SRVideoPlayer.h"
#import "Base.h"

@interface VideoPlayOfflineVC ()
{
    NSURL *url;
}
@property (nonatomic, strong) SRVideoPlayer *videoPlayer;
@end

@implementation VideoPlayOfflineVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    [self showVideoPlayer];
}	
- (void)viewDidDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_videoPlayer destroyPlayer];
}

- (void)showVideoPlayer {
    UIView *playerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.width)];
    playerView.center = self.view.center;
    [self.view addSubview:playerView];
    
    self.videolbl.text=_videolabelStr;
    self.videopath.text=_videoPath;
    NSLog(@"video path=%@ video lbl=%@",_videoPath,_videolabelStr);
    
    NSURL * localURL = [NSURL fileURLWithPath:_videoPath];
    NSLog(@"****localURL :%@",localURL);
    
   _videoPlayer = [SRVideoPlayer playerWithVideoURL:localURL playerView:playerView playerSuperView:playerView.superview];
  
    [_videoPlayer play];
}


@end
