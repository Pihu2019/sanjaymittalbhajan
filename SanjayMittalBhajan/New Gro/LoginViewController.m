
 #import "Reachability.h"
 #import "LoginViewController.h"
 #import "Constant.h"
 #import "Base.h"
 #import "Login.h"
 #import "Role.h"
 #import "AfterLoginHomeViewController.h"
 @interface LoginViewController ()
 @end

@implementation LoginViewController
 
 - (void)viewDidLoad {
 [super viewDidLoad];
 [_textFieldUsername becomeFirstResponder];
 
 _textFieldPassword.delegate=self;
 _textFieldUsername.delegate=self;
 [self.indicator setHidden:YES];
 
 UIColor *color = [UIColor grayColor];
 _textFieldUsername.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
 _textFieldPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
 
 
 self.imageViewLogin.layer.cornerRadius = self.imageViewLogin.frame.size.width / 2;
 self.imageViewLogin.clipsToBounds = YES;
 
 [self testInternetConnection];
 
 UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_loginView.bounds
 byRoundingCorners:(UIRectCornerBottomRight | UIRectCornerBottomLeft)
 cornerRadii:CGSizeMake(8.0, 8.0)];
 
 CAShapeLayer *maskLayer = [CAShapeLayer layer];
 maskLayer.frame = _loginView.bounds;
 maskLayer.path = maskPath.CGPath;
 _loginView.layer.mask = maskLayer;
 
 }
 - (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
 {
 [self.textFieldUsername resignFirstResponder];
 [self.textFieldPassword resignFirstResponder];
 }
 
 - (BOOL)textFieldShouldReturn:(UITextField *)textField
 {
 if(textField==self.textFieldUsername)
 {
 [textField resignFirstResponder];
 [self.textFieldPassword becomeFirstResponder];
 }
 else
 {
 [textField resignFirstResponder];
 }
 return  YES;
 }
 
 - (IBAction)loginclicked:(id)sender {
 
 
 [_indicator startAnimating];
 
 if(self.textFieldUsername.text.length==0 && self.textFieldPassword.text.length==0)
 {
 UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:@"Enter username and password" preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* ok = [UIAlertAction
 actionWithTitle:@"OK"
 style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 [alertView dismissViewControllerAnimated:YES completion:nil];
 
 }];
 
 [alertView addAction:ok];
 [_indicator stopAnimating];
 [self.indicator setHidden:YES];
 
 [self presentViewController:alertView animated:YES completion:nil];
 
 }
 if(self.textFieldUsername.text.length==0)
 {
 UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:@"First enter your username" preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* ok = [UIAlertAction
 actionWithTitle:@"OK"
 style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 [alertView dismissViewControllerAnimated:YES completion:nil];
 
 }];
 
 [alertView addAction:ok];
 [_indicator stopAnimating];
 [self.indicator setHidden:YES];
 
 [self presentViewController:alertView animated:YES completion:nil];
 }
 if(self.textFieldPassword.text.length==0)
 {
 UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:@"First enter your password" preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* ok = [UIAlertAction
 actionWithTitle:@"OK"
 style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 [alertView dismissViewControllerAnimated:YES completion:nil];
 
 }];
 
 [alertView addAction:ok];
 [_indicator stopAnimating];
 [self.indicator setHidden:YES];
 
 [self presentViewController:alertView animated:YES completion:nil];
 }
 
 if(self.textFieldUsername.text.length>0 && self.textFieldPassword.text.length>0)
 {
 NSLog(@"hi");
 
 [self requestLogindata];
 }
 }
 
 
 -(void)requestLogindata{
 
 NSDictionary *data = @{ @"username":self.textFieldUsername.text ,
 @"password": self.textFieldPassword.text
 };
 
 
 [[NSUserDefaults standardUserDefaults] setObject:self.textFieldUsername.text forKey:@"username"];
 [[NSUserDefaults standardUserDefaults] synchronize];
 
 
 NSError *error;
 NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:kNilOptions error:&error];
 
 NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:login]]];
 
 NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url cachePolicy:nil timeoutInterval:60];
 [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
 [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
 [req setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
 [req setHTTPMethod:@"POST"];
 [req setHTTPBody:jsonData];
 
 NSString *retStr = [[NSString alloc] initWithData:[NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil] encoding:NSUTF8StringEncoding];
 
 NSLog(@"Return String%@",retStr);
 
 NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:[NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil] options:NSJSONReadingMutableContainers error:nil];
 NSLog(@"response data:%@",maindic);
 
 
 self.status=[maindic objectForKey:@"status"];
 self.message=[maindic objectForKey:@"message"];
 
 NSArray *detailArr=[maindic objectForKey:@"details"];
 
 NSLog(@"status==%@& message=%@ details==%@",self.status,self.message,detailArr);
 [[NSUserDefaults standardUserDefaults] setObject:self.status forKey:@"status"];
 [[NSUserDefaults standardUserDefaults] synchronize];
 
 [[NSOperationQueue mainQueue]addOperationWithBlock:^{
 
 }];
 if ([self.status isEqual:@"0"]) {
 
 UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:@"Entered credential is incorrect" preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* ok = [UIAlertAction
 actionWithTitle:@"OK"
 style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 [alertView dismissViewControllerAnimated:YES completion:nil];
 
 }];
 
 [alertView addAction:ok];
 [self presentViewController:alertView animated:YES completion:nil];
 [_indicator stopAnimating];
 [self.indicator setHidden:YES];
 }
 else{
 
 NSDictionary *dic=[maindic objectForKey:@"body"];
 
 NSLog(@"user login dic%@",dic);
 
 
 Login *c=[[Login alloc]init];
 
 c.userId=[dic objectForKey:@"userId"];
 c.username=[dic objectForKey:@"username"];
 c.address=[dic objectForKey:@"address"];
 c.email=[dic objectForKey:@"email"];
 c.contactNum=[dic objectForKey:@"contactNumber"];
 c.enabled=[dic objectForKey:@"enabled"];
 c.userrole=[dic objectForKey:@"userRole"];
 
 
 [[NSUserDefaults standardUserDefaults] setObject:c.username forKey:@"profileusername"];
 [[NSUserDefaults standardUserDefaults] synchronize];
 
 [[NSUserDefaults standardUserDefaults] setObject:c.address forKey:@"address"];
 [[NSUserDefaults standardUserDefaults] synchronize];
 
 [[NSUserDefaults standardUserDefaults] setObject:c.email forKey:@"email"];
 [[NSUserDefaults standardUserDefaults] synchronize];
 
 [[NSUserDefaults standardUserDefaults] setObject:c.contactNum forKey:@"mobile"];
 [[NSUserDefaults standardUserDefaults] synchronize];
 
 
 
 NSLog(@"Username==%@ & Userrole==%@",c.username,c.userrole);
 [self navigatingFromLogin];
 }
 }
 -(void)navigatingFromLogin{
 
 NSString *status = [[NSUserDefaults standardUserDefaults]
 stringForKey:@"status"];
 NSLog(@"status==%@",status);
 
 if (status!=NULL)
 {
 if ([status isEqual:@"1"])
 {
 AfterLoginHomeViewController *admin = [self.storyboard instantiateViewControllerWithIdentifier:@"AfterLogin"];
 
 [self.navigationController pushViewController:admin animated:YES];
 }
 else
 {
 UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"Entered credential is incorrect" preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* ok = [UIAlertAction
 actionWithTitle:@"OK"
 style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 [alertView dismissViewControllerAnimated:YES completion:nil];
 
 }];
 
 [alertView addAction:ok];
 [self presentViewController:alertView animated:YES completion:nil];
 [_indicator stopAnimating];
 [self.indicator setHidden:YES];
 }
 }
 }
 - (void)testInternetConnection
 {
 __weak typeof(self) weakSelf = self;
 NSLog(@"%@",weakSelf);
 Reachability *internetReachable = [Reachability reachabilityForInternetConnection];
 // NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
 
 internetReachable = [Reachability reachabilityWithHostname:@"www.google.com"];
 
 // Internet is reachable
 internetReachable.reachableBlock = ^(Reachability*reach)
 {
 // Update the UI on the main thread
 dispatch_async(dispatch_get_main_queue(), ^{
 //Make sure user interaction on whatever control is enabled
 });
 };
 
 // Internet is not reachable
 internetReachable.unreachableBlock = ^(Reachability*reach)
 {
 // Update the UI on the main thread
 dispatch_async(dispatch_get_main_queue(), ^{
 
 UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"No Internet Connection" message:@"Please connect to the internet." preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* ok = [UIAlertAction
 actionWithTitle:@"OK"
 style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 [alertView dismissViewControllerAnimated:YES completion:nil];
 }];
 [alertView addAction:ok];
 [self presentViewController:alertView animated:YES completion:nil];
 //Make sure user interaction on whatever control is disabled
 });
 };
 
 [internetReachable startNotifier];
 }
 
 @end


