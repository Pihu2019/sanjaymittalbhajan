

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AudioListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backgroundViewCell;
@property (weak, nonatomic) IBOutlet UIImageView *audioImg;
@property (weak, nonatomic) IBOutlet UILabel *audioTitle;
@property (weak, nonatomic) IBOutlet UILabel *thumbnailPath;
@property (weak, nonatomic) IBOutlet UILabel *audioPath;
@property (weak, nonatomic) IBOutlet UILabel *audioSize;
@property (weak, nonatomic) IBOutlet UILabel *tags;
@property (weak, nonatomic) IBOutlet UILabel *fileId;
@property (weak, nonatomic) IBOutlet UILabel *fileName;
@property (weak, nonatomic) IBOutlet UIButton *playPauseBtn;




@end

NS_ASSUME_NONNULL_END
