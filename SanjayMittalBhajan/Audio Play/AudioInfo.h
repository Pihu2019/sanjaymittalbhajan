//
//  AudioInfo.h
//  SanjayMittalBhajan
//
//  Created by Punit on 07/12/18.
//  Copyright © 2018 Eshiksa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AudioInfo : NSObject

@property(nonatomic,retain)NSString *bhajanTitleStr,*audioPathStr,*thumbnailPathStr,*fileName,*audioSize,*pdfFileName,*fileId,*tag;

@end

NS_ASSUME_NONNULL_END
