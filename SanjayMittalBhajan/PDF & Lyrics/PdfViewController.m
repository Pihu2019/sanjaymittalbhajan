


#import "PdfViewController.h"
#import "Base.h"
#import "PdfTableViewCell.h"
#import "WebViewController.h"
#import "LyricsDetailViewController.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "DGActivityIndicatorView.h"
#define CGRect pageFrame = CGRectMake(0,0,595.2,841,8);
@interface PdfViewController ()
{
    BOOL isFiltered;
    int startPage;
    int lastPage ;
    
    NSURL *finalURL;
    NSString *fileurl;
    DGActivityIndicatorView *activityIndicatorView;

}
@end

@implementation PdfViewController
@synthesize searchBar;
- (void)viewDidLoad {
    [super viewDidLoad];
    startPage = 1 ;
    lastPage = 1 ;
    [self.tableview setSeparatorColor:[UIColor clearColor]];
    
     searchBar.delegate=(id)self;
    _tableview.delegate=self;
    _tableview.dataSource=self;
    _pdfListArr=[[NSMutableArray alloc]init];

    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"Alert!" message:@"Please check internet connection" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action=[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               
                               {
                               }];
        
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.tableview animated:NO];
        hud.mode = MBProgressHUDAnimationFade;
        hud.labelText = @"Loading...";
        
        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor]];
        CGFloat width = self.view.bounds.size.width / 6.0f;
        CGFloat height = self.view.bounds.size.height / 6.0f;
        activityIndicatorView.frame = CGRectMake(20,40,width,height);
        
         [self datapdfParsing];
    }
}


-(void)viewDidUnload{
    [self setSearchBar:nil];
    [super viewDidUnload];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"****searchText:%@",searchText);
    
    if(searchText.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        isFiltered = true;
             _filteredArray=[[NSMutableArray alloc]init];
        for (NSDictionary *temp in _pdfListArr)
        {
            NSRange nameRange = [[[temp objectForKey:@"tags"]description] rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(nameRange.location != NSNotFound)
            {
                [_filteredArray addObject:temp];
            }
        }
    }
    
    [self.tableview reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.tableview resignFirstResponder];
}
-(void)datapdfParsing{
    if (startPage<=lastPage) {
        NSLog(@"start page=== %d",startPage) ;
      
    queue=dispatch_queue_create("images", DISPATCH_QUEUE_CONCURRENT);
    queue=dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0);
    
    //[_pdfListArr removeAllObjects];
    NSString *username = @"priya0102";
    NSString *password = @"priya12345";
    
    NSString *unpw = [NSString stringWithFormat:@"%@:%@",username,password];
    NSData *updata = [unpw dataUsingEncoding:NSASCIIStringEncoding];
    
    NSString *base64str = [NSString stringWithFormat:@"Basic %@", [updata base64Encoding]];
    NSDictionary *headers = @{ @"content-type": @"json/application",
                               @"authorization": base64str };

    NSString *mainstr=[NSString stringWithFormat:@"http://35.200.153.165:8080/Sanjay_Mittal_Bhajans/apis/pdf/pdf-list?pageNo=%d",startPage];
    
    NSLog(@"*****url string==%@",mainstr);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:mainstr]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (error) {
            NSLog(@"%@", error);
        }
        else {
            
            NSLog(@"Success: %@", data);
            
            NSError *err;
            
            NSArray *jsonArray  = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            NSLog(@"JSON DATA%@",jsonArray);
          
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:[NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil] options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"response data:%@",maindic);
            
            self.status=[maindic objectForKey:@"status"];
            self.message=[maindic objectForKey:@"message"];
            
            NSArray *detailArr=[maindic objectForKey:@"details"];
            NSDictionary *bodyDic=[maindic objectForKey:@"body"];
            
            self.currentPageNo=[bodyDic objectForKey:@"currentPageNo"];
            self.totalCount=[bodyDic objectForKey:@"totalCount"];
           // self.totalPageSize=[bodyDic objectForKey:@"totalPageSize"];
             lastPage = [bodyDic[@"totalPageSize"] intValue];
            NSLog(@"totalcount==%@, totalpagesize==%d",self.totalCount,lastPage);
            
            NSArray *bodyArr=[bodyDic objectForKey:@"body"];
            
            
            if(bodyArr.count==0)
            {
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"Currently there is no pdf data." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alertView dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
                
                
                [alertView addAction:ok];
                
                [self presentViewController:alertView animated:YES completion:nil];
                
            }
            else {
                
                for(NSDictionary *temp in bodyArr)
                {
                    NSString *str1=[[temp objectForKey:@"thumbnailsPath"]description];
                    NSString *str2=[[temp objectForKey:@"pdfPath"]description];
                    NSString *str3=[[temp objectForKey:@"fileName"]description];
                    NSString *str4=[[temp objectForKey:@"pdfSize"]description];
                    NSString *str5=[[temp objectForKey:@"tags"]description];
                    NSString *str6=[[temp objectForKey:@"bhajanTitle"]description];
                    NSString *str7=[[temp objectForKey:@"lyricsType"]description];
                    NSString *str8=[[temp objectForKey:@"textLyrics"]description];
                    
                    NSLog(@"thumbnailsPath=%@  pdfPath=%@ fileName=%@ pdfSize=%@  tags=%@  bhajanTitle=%@lyricsType=%@ textLyrics=%@",str1,str2,str3,str4,str5,str6,str7,str8);
                    
                    [self.pdfListArr addObject:temp];
                    NSLog(@"_pdf ListArr ARRAYY%@",self.pdfListArr);
                }
            }
    NSLog(@"count: %lu",(unsigned long)self.pdfListArr.count) ;
             [self.tableview reloadData];
          [MBProgressHUD hideHUDForView:self.tableview animated:NO];
        }
                           });
    }];
    [dataTask resume];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger rowCount;
    if (isFiltered)
        rowCount=_filteredArray.count;
    else
        rowCount=_pdfListArr.count;
    return  rowCount;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PdfTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if (isFiltered) {
        NSMutableDictionary *ktemp=[_filteredArray objectAtIndex:indexPath.row];
        
        cell.bhajantitle.text=[[ktemp objectForKey:@"bhajanTitle"]description];
        cell.thumbnailPath.text=[[ktemp objectForKey:@"thumbnailsPath"]description];
        cell.pdfPath.text=[[ktemp objectForKey:@"pdfPath"]description];
        cell.pdfSize.text=[[ktemp objectForKey:@"pdfSize"]description];
        cell.tags.text=[[ktemp objectForKey:@"tags"]description];
        cell.lyricsInfo.text=[[ktemp objectForKey:@"textLyrics"]description];
        cell.lyricsType.text=[[ktemp objectForKey:@"lyricsType"]description];
        cell.fileName.text=[[ktemp objectForKey:@"fileName"]description];
        
     _pdfPath = [imgUrl stringByAppendingString:[[ktemp objectForKey:@"pdfPath"]description]];
    _addStr=[_pdfPath stringByAppendingString:@"/"];
        
    _filenameStr=[_pdfPath stringByAppendingString:[[ktemp objectForKey:@"lyricsType"]description]];
        
        cell.downloadUrl.text=_filenameStr;

        NSString *urlstr = [imgUrl stringByAppendingString:[[ktemp objectForKey:@"thumbnailsPath"]description]];
        NSLog(@"URL PATH=%@",urlstr);
        if ([urlstr containsString:@"<null>"]) {
            cell.pdfImg.image=[UIImage imageNamed:@"pdf_background.png"];
        }
        else{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *img1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:     [NSURL URLWithString:urlstr]]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                cell.pdfImg.image = img1;
            });
        });
        }
        return cell;
    
    }else{

    
    NSMutableDictionary *ktemp=[_pdfListArr objectAtIndex:indexPath.row];
    
    cell.bhajantitle.text=[[ktemp objectForKey:@"bhajanTitle"]description];
    cell.thumbnailPath.text=[[ktemp objectForKey:@"thumbnailsPath"]description];
    cell.pdfPath.text=[[ktemp objectForKey:@"pdfPath"]description];
    cell.pdfSize.text=[[ktemp objectForKey:@"pdfSize"]description];
    cell.tags.text=[[ktemp objectForKey:@"tags"]description];
    cell.lyricsInfo.text=[[ktemp objectForKey:@"textLyrics"]description];
    cell.lyricsType.text=[[ktemp objectForKey:@"lyricsType"]description];
    cell.fileName.text=[[ktemp objectForKey:@"fileName"]description];
        
    cell.pdfButton.tag=indexPath.row;
        
    if ([cell.lyricsType.text isEqualToString:@"TEXT"]) {
            [cell.pdfButton addTarget:self action:@selector(textButtonclicked:) forControlEvents:UIControlEventTouchUpInside];
        }
    else if ([cell.lyricsType.text isEqualToString:@"PDF"]){
        
      [cell.pdfButton addTarget:self action:@selector(pdfButtonclicked:) forControlEvents:UIControlEventTouchUpInside];
        }
        
    _pdfPath = [imgUrl stringByAppendingString:[[ktemp objectForKey:@"pdfPath"]description]];
      
    _addStr=[_pdfPath stringByAppendingString:@"/"];
        
        
    _filenameStr=[_addStr stringByAppendingString:[[ktemp objectForKey:@"lyricsType"]description]];
        
    cell.downloadUrl.text=_filenameStr;
        
        NSLog(@"PDF STR==%@",_filenameStr);


        [[NSUserDefaults standardUserDefaults] setObject:_filenameStr forKey:@"filenameStr"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *urlstr = [imgUrl stringByAppendingString:[[ktemp objectForKey:@"thumbnailsPath"]description]];
    NSLog(@"URL PATH=%@",urlstr);
    if ([urlstr containsString:@"<null>"]) {
         cell.pdfImg.image=[UIImage imageNamed:@"pdf_background.png"];
    }
    else{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *img1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:     [NSURL URLWithString:urlstr]]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
           cell.pdfImg.image = img1;
            
        });
    });
        }
        if (indexPath.row == _pdfListArr.count-1) {
            startPage = startPage + 1 ;
            [self datapdfParsing] ;
        }
        return cell;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    PdfTableViewCell *cell=[_tableview cellForRowAtIndexPath:indexPath];
    NSLog(@"cell==%@",cell);
    _indxp=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
    _lyricsTitle=cell.bhajantitle.text;
    _lyricsInfo=cell.lyricsInfo.text;
    _lyricsTypeStr=cell.lyricsType.text;
    _filenameStr=cell.downloadUrl.text;
    
    
            [[NSUserDefaults standardUserDefaults] setObject:_filenameStr forKey:@"pdfstr"];
            [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    NSLog(@"indexpath==%ld & lyrics type=%@ pdf path=%@ full pdf url=%@",(long)indexPath.row,_lyricsTypeStr,_pdfPath,_pdfPath);
    
    if([_lyricsTypeStr isEqualToString:@"PDF"])
    {
        [self performSegueWithIdentifier:@"showPdf"
                                  sender:[self.tableview cellForRowAtIndexPath:indexPath]];
    }
    else if([_lyricsTypeStr isEqualToString:@"TEXT"])
    {
        [self performSegueWithIdentifier:@"showLyrics"
                                  sender:[self.tableview cellForRowAtIndexPath:indexPath]];
    }
    
   
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   return 90;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    

    if ([segue.identifier isEqualToString:@"showPdf"]) {

      WebViewController *wvc=[segue destinationViewController];
        NSString *pdfstr = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"pdfstr"];
        NSLog(@"***pdfstr ==%@",pdfstr);

        wvc.myURL=pdfstr;

        NSLog(@"*******full downloading url str=%@",pdfstr);
    }
    
    if ([[segue identifier] isEqualToString:@"showLyrics"])
    {
        
        LyricsDetailViewController *kvc = [segue destinationViewController];
        
        kvc.lyricsTitleStr=_lyricsTitle;
        kvc.lyricsInfoStr=_lyricsInfo;
        kvc.indxpath=_indxp;
        
        NSLog(@"indexpath in prepare for segue==%@",_lyricsTitle);
    }
}


-(void)pdfButtonclicked:(UIButton*)sender{
    
    NSString *emailid=[[NSUserDefaults standardUserDefaults]stringForKey:@"username"];
    NSLog(@"email id==%@",emailid);
    
    UIButton *btn =(UIButton *)sender;
    NSLog(@"Btn Click...........%ld",(long)btn.tag);
    indexBtn=sender.tag;
    NSLog(@"INDEX video===%ld",indexBtn);
    
    if (emailid==NULL) {
        UIAlertController * alert=[UIAlertController
                                   
                                   alertControllerWithTitle:@"Sorry!" message:@"You can't download media because you are not login.For downloading you need to login first!!"preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        NSLog(@"you pressed ok, please button");
                                        
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
          [self savePdfInDocumentsDirectory];
          //[self showAlert2:@"Alert" :@"File downloaded!"];
    }
    
}

-(NSData *)savePdfInDocumentsDirectory
{
    NSMutableDictionary *ktemp=[_pdfListArr objectAtIndex:indexBtn];
    
    _pdfPath = [imgUrl stringByAppendingString:[[ktemp objectForKey:@"pdfPath"]description]];
    
    _addStr=[_pdfPath stringByAppendingString:@"/"];
    
    NSString *filePDF=[_addStr stringByAppendingString:[[ktemp objectForKey:@"fileName"]description]];
    NSLog(@"fileurl:::%@",filePDF);
    
    
     NSURL *finalpdfURL = [NSURL URLWithString:[filePDF stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    NSLog(@"***finalURLString***%@",finalpdfURL);
    
    NSData *pdfdata = [NSData dataWithContentsOfURL:finalpdfURL];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsPath = [paths objectAtIndex:[_indexp intValue]];
    
    NSString *pdffilePath = [documentsPath stringByAppendingPathComponent:[[filePDF componentsSeparatedByString:@"/"]lastObject]];
    NSLog(@"**filePath %@",pdffilePath);
    
    [pdfdata writeToFile:pdffilePath atomically:YES];
    
    
    printf("pdf file == %s",[pdffilePath UTF8String]);
    
    
    UISaveVideoAtPathToSavedPhotosAlbum (pdffilePath,self,@selector(video:didFinishSavingWithError:contextInfo:),nil);
    
    return pdfdata;
    
   
}
- (void) video: (NSString *) videoPath didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    NSLog(@"Finished saving pdf with error: %@", error);
    
}
-(void)fetchAudioInDocumentsDirectory:(NSString*)filepath
{
    data = [NSData dataWithContentsOfFile:filePath];
    NSLog(@"dataWithContentsOfFile:%@",data);
    
}
- (IBAction)viewDownloadBtnClciked:(id)sender {
    
    NSLog(@"fetchImageInDocumentsDirectory:");
    [self fetchAudioInDocumentsDirectory:filePath];
    
}

-(void)textButtonclicked:(UIButton*)sender{
    NSString *emailid=[[NSUserDefaults standardUserDefaults]stringForKey:@"username"];
    NSLog(@"email id==%@",emailid);
    
    UIButton *btn =(UIButton *)sender;
    NSLog(@"text Btn Click...........%ld",(long)btn.tag);
    indexBtn=sender.tag;
    NSLog(@"INDEX video===%ld",indexBtn);
    
    if (emailid==NULL) {
        UIAlertController * alert=[UIAlertController
                                   
                                   alertControllerWithTitle:@"Sorry!" message:@"You can't download media because you are not login.For downloading you need to login first!!"preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        NSLog(@"you pressed ok, please button");
                                        
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        [self writeToTextFile];
        [self showAlert:@"Alert" :@"File downloaded!"];
   }
}

-(void)writeToTextFile{
    
    NSMutableDictionary *ktemp=[_pdfListArr objectAtIndex:indexBtn];
    
    NSString *file=[[ktemp objectForKey:@"textLyrics"]description];
    
    NSString *bhajantitle=[[ktemp objectForKey:@"bhajanTitle"]description];
    
    NSString *str1=[bhajantitle stringByAppendingFormat:@".txt"];
    NSLog(@"***str1***%@",str1);

    NSLog(@"fileurl:::%@",file);

    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:[_indexp intValue]];
    
     filePath = [documentsDirectory stringByAppendingPathComponent:str1];
    
    NSLog(@"fileName %@",filePath);

    [file writeToFile:filePath atomically:YES];
    

    
}

-(void)showAlert:(NSString *)title :(NSString*)message{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}
-(void)showAlert2:(NSString *)title :(NSString*)message{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
