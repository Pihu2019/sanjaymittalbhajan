//
//  LyricsDetailViewController.m
//  SanjayMittalBhajan
//
//  Created by Punit on 14/11/18.
//  Copyright © 2018 Eshiksa. All rights reserved.
//

#import "LyricsDetailViewController.h"

@interface LyricsDetailViewController ()

@end

@implementation LyricsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lyricsTitle.text=self.lyricsTitleStr;
    self.lyricsInfo.text=self.lyricsInfoStr;
    
    [_scrollview setShowsHorizontalScrollIndicator:NO];
    [_scrollview setShowsVerticalScrollIndicator:NO];
    
    NSLog(@"LYRICS TITLE==%@ & LYRICS INFO=%@",_lyricsTitleStr,_lyricsInfoStr);
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_contentView.bounds
                                                   byRoundingCorners:(UIRectCornerBottomRight | UIRectCornerBottomLeft)
                                                         cornerRadii:CGSizeMake(8.0, 8.0)];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = _contentView.bounds;
    maskLayer.path = maskPath.CGPath;
    _contentView.layer.mask = maskLayer;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
