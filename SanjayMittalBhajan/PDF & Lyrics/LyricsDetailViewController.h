//
//  LyricsDetailViewController.h
//  SanjayMittalBhajan
//
//  Created by Punit on 14/11/18.
//  Copyright © 2018 Eshiksa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LyricsDetailViewController : UIViewController
@property(nonatomic,retain)NSString *lyricsTitleStr,*lyricsInfoStr,*lyricsType,*indxpath;

@property (weak, nonatomic) IBOutlet UILabel *lyricsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lyricsInfo;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;

@end

NS_ASSUME_NONNULL_END
