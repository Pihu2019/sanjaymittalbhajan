//
//  PdfViewController.h
//  SanjayMittalBhajan
//
//  Created by Punit on 06/08/18.
//  Copyright © 2018 Eshiksa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebConnection1.h"
#import "AppDelegate.h"
@interface PdfViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLSessionDelegate,UITextViewDelegate,UISearchBarDelegate,UISearchControllerDelegate>
    {
        dispatch_queue_t queue;
        NSInteger indexBtn;
        NSData *data;
        NSString *filePath;
        
//        WebConnection1 *connection;
//        NSMutableDictionary *dict;
//        AppDelegate *appDelegate;
//        NSMutableArray *pdfListArr;
//        int page;
//        BOOL isPageRefreshing;
    }
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic,strong) NSMutableArray *pdfListArr,*filteredArray;

@property(nonatomic,retain)NSString *indxp,*status,*message,*details,*currentPageNo,*totalCount,*totalPageSize,*lyricsTitle,*lyricsInfo,*lyricsTypeStr,*pdfPath,*filenameStr,*addStr,*indexp;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;


@end
