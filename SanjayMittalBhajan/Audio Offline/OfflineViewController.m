//https://www.appcoda.com/sqlite-database-ios-app-tutorial/
//internal play path how to play

#import "OfflineViewController.h"
#import "OfflineAudioTableViewCell.h"
#import "Base.h"
#import "OfflineAudioPlayViewController.h"
@interface OfflineViewController ()
{
    int indxp;
    NSArray *filePathsArray;
    BOOL isDataLoading;
    NSString *path;
}
@end

@implementation OfflineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableview.delegate=self;
    _tableview.dataSource=self;
        [self.tableview setSeparatorColor:[UIColor clearColor]];
    
    
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
       // filePathsArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory  error:nil];
     filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
    
      _arrMp3Files=[[NSMutableArray alloc]init];
    
        for (NSString *str in filePathsArray) {
            NSString *strFileName=[str.lastPathComponent lowercaseString];
            if([strFileName.pathExtension isEqualToString:@"mp3"])
            {
                NSLog(@"stringggg   %@",str);
                [_arrMp3Files addObject:str];
                NSLog(@"arrMp3Files  %@",_arrMp3Files);
            }
        }
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return  _arrMp3Files.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    OfflineAudioTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];

    filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
    path = [documentsDirectory stringByAppendingPathComponent:[_arrMp3Files objectAtIndex:indexPath.row]];
   
     NSLog(@"path===%@", path);
    cell.audiofilePath.text=path;
    NSURL *url = CFBridgingRelease(CFURLCreateWithFileSystemPath(kCFAllocatorDefault, (CFStringRef)path, kCFURLWindowsPathStyle, false));
    NSString *fileName = url.lastPathComponent;
     NSLog(@"fileName===%@",fileName);
    
    NSRange range = [fileName rangeOfString:@"."];
    if (range.location != NSNotFound) {
        cell.audioTitle.text = [fileName substringToIndex:range.location];
        NSLog(@" cell.audioTitle.text %@", cell.audioTitle.text);
    } else {
        NSLog(@". is not found");
    }
    
    return cell;

    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 106;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    OfflineAudioTableViewCell *cell=[_tableview cellForRowAtIndexPath:indexPath];
    
   _bhajanTitle=cell.audioTitle.text;
    path=cell.audiofilePath.text;

    indxp=indexPath.row;

    NSLog(@"audio indexpath==%ld audiopath=%@  path=%@",(long)indexPath.row,_bhajanTitle,path);



    dispatch_async( dispatch_get_main_queue(),
                   ^{
                       [self performSegueWithIdentifier:@"offlineAudioPlay"
                                                 sender:[self.tableview cellForRowAtIndexPath:indexPath]];
                   });
   
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"offlineAudioPlay"])
    {

        OfflineAudioPlayViewController *kvc = [segue destinationViewController];
        
        kvc.audiopathStr=path;
        kvc.audioTitleStr=_bhajanTitle;
        kvc.selectedIndex=indxp;
        kvc.audiopathArray = [_arrMp3Files mutableCopy];
        
        NSLog(@"_audioPath==%@ & _bhajanTitle =%@ selected index =%d ",path,_bhajanTitle,indxp);

    }
}


@end
