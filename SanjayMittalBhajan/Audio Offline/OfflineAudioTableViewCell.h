//
//  OfflineAudioTableViewCell.h
//  SanjayMittalBhajan
//
//  Created by Punit on 17/12/18.
//  Copyright © 2018 Eshiksa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OfflineAudioTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *audioTitle;
@property (weak, nonatomic) IBOutlet UILabel *audiofilePath;
@end

NS_ASSUME_NONNULL_END
