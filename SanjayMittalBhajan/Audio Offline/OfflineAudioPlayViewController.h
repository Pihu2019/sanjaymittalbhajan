
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AudioToolbox/AudioToolbox.h>
NS_ASSUME_NONNULL_BEGIN

@interface OfflineAudioPlayViewController : UIViewController<AVAudioPlayerDelegate>
{
    dispatch_queue_t queue;
    //AVPlayer *player;
    NSTimer *playbackTimer;
}
@property (weak, nonatomic) IBOutlet UILabel *audioTitle;
@property (weak, nonatomic) IBOutlet UILabel *audioLocalPath;

@property (weak, nonatomic) IBOutlet UIView *waveView;
@property (weak, nonatomic) IBOutlet UIButton *playPauseBtn;
- (IBAction)progressSliderChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UISlider *progressSlider;
@property (weak, nonatomic) IBOutlet UILabel *currentTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *kimgview;
- (IBAction)nextBtnClicked:(id)sender;
- (IBAction)previousBtnClicked:(id)sender;

//@property(strong,nonatomic)AVAudioPlayer *audioPlayer;

@property(strong,nonatomic)NSTimer *sliderTimer;
-(NSString *)stringFromInterval:(NSTimeInterval)interval;
-(void)updateSlider;
-(void)playSongWithURL:(NSURL *)audioUrl;

@property(nonatomic,retain)NSString *audiopathStr,*audioTitleStr,*indxpath;

@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIButton *previousBtn;
@property(nonatomic,readwrite) int selectedIndex,nextIndex,previousIndex;
//-(void) setupAVPlayerForURL: (NSURL*) url;

@property(nonatomic,retain)NSMutableArray *audiopathArray;

@end

NS_ASSUME_NONNULL_END
