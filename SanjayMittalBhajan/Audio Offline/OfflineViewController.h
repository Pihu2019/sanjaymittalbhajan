//file get absolute path

//  OfflineViewController.h
//  SanjayMittalBhajan
//
//  Created by Punit on 30/11/18.
//  Copyright © 2018 Eshiksa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OfflineViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
   AVAudioPlayer *player;
    BOOL isPlaying;
}

@property(nonatomic,retain)NSString *indexp,*bhajanTitle;
@property(nonatomic,retain)UIImage *audioImg;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic, strong) NSMutableArray *arrMp3Files;



@end

NS_ASSUME_NONNULL_END
