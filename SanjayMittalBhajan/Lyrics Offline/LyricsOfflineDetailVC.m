

#import "LyricsOfflineDetailVC.h"

@interface LyricsOfflineDetailVC ()

@end

@implementation LyricsOfflineDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lyricsTitle.text=self.lyricsTitleStr;

    [_scrollview setShowsHorizontalScrollIndicator:NO];
    [_scrollview setShowsVerticalScrollIndicator:NO];

    NSString *contents = [NSString stringWithContentsOfFile:self.lyricsInfoStr ];
    self.lyricsInfo.text=contents;
 
     NSLog(@"LYRICS TITLE==%@ & LYRICS  INFO=%@",_lyricsTitleStr,_lyricsInfoStr);
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_contentView.bounds
                                                   byRoundingCorners:(UIRectCornerBottomRight | UIRectCornerBottomLeft)
                                                         cornerRadii:CGSizeMake(8.0, 8.0)];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = _contentView.bounds;
    maskLayer.path = maskPath.CGPath;
    _contentView.layer.mask = maskLayer;
}


@end
