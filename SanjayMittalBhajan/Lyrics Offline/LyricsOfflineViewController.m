
    #import "LyricsOfflineViewController.h"
    #import "LyricsOfflineTableViewCell.h"
    #import "LyricsOfflineDetailVC.h"
    #import "PDFKBasicPDFViewer.h"
    #import "PDFKDocument.h"

    @interface LyricsOfflineViewController ()
    {
         NSArray *filePathsArray;
           BOOL isDataLoading;
        NSString *path;
         NSInteger indxp;
    }
    @end

    @implementation LyricsOfflineViewController

    - (void)viewDidLoad {
        [super viewDidLoad];
        
        _tableview.delegate=self;
        _tableview.dataSource=self;
        [self.tableview setSeparatorColor:[UIColor clearColor]];
       
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
        
        _arrLyricsFiles=[[NSMutableArray alloc]init];
        
        for (NSString *str in filePathsArray) {
            NSString *strFileName=[str.lastPathComponent lowercaseString];
            if([strFileName.pathExtension isEqualToString:@"txt"])
            {
                NSLog(@"stringggg   %@",str);
                [_arrLyricsFiles addObject:str];
                NSLog(@"_arrLyricsFiles text  %@",_arrLyricsFiles);
            }
            if([strFileName.pathExtension isEqualToString:@"pdf"])
            {
                NSLog(@"stringggg   %@",str);
                [_arrLyricsFiles addObject:str];
                NSLog(@"_arrLyricsFiles pdf  %@",_arrLyricsFiles);
            }
            
        }
    }
    -(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
    {
        return 1;
    }

    -(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
    {

        return  _arrLyricsFiles.count;
    }

    -(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        
        LyricsOfflineTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
      
        filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
        
        path  = [documentsDirectory stringByAppendingPathComponent:[_arrLyricsFiles objectAtIndex:indexPath.row]];
        
        NSLog(@"path===%@", path);
        cell.lyricspath.text=path;
        
        NSURL *url = CFBridgingRelease(CFURLCreateWithFileSystemPath(kCFAllocatorDefault, (CFStringRef)path, kCFURLWindowsPathStyle, false));
        NSString *fileName = url.lastPathComponent;
        NSLog(@"fileName===%@",fileName);
        
        NSRange range = [fileName rangeOfString:@"."];
        if (range.location != NSNotFound) {
            cell.lyricsTitle.text = [fileName substringToIndex:range.location];
            NSLog(@"lyricsTitle %@", cell.lyricsTitle.text);
        } else {
            NSLog(@". is not found");
        }
        
        return cell;
        
       
    }
    - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        return 106;
    }
    -(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
    {
        LyricsOfflineTableViewCell *cell=[_tableview cellForRowAtIndexPath:indexPath];
        NSLog(@"cell==%@",cell);
        
        _lyricsTitle=cell.lyricsTitle.text;
        _lyricsPath=cell.lyricspath.text;
        indxp=indexPath.row;
        
        [[NSUserDefaults standardUserDefaults] setObject:_lyricsPath forKey:@"lyricsPath"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
      
        
           NSLog(@" indexpath==%ld _lyricsTitle=%@  _lyricsPath=%@",(long)indexPath.row,_lyricsTitle,_lyricsPath);
        
        if([_lyricsPath containsString:@".pdf"])
        {
            [self performSegueWithIdentifier:@"showPDFOffline"
                                      sender:[self.tableview cellForRowAtIndexPath:indexPath]];
        }
        else if([_lyricsPath containsString:@".txt"])
        {
            [self performSegueWithIdentifier:@"showLyricsOffline"
                                      sender:[self.tableview cellForRowAtIndexPath:indexPath]];
        }
        
        
    }

    - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
        
        
        if ([segue.identifier isEqualToString:@"showPDFOffline"]) {
            
            PDFKBasicPDFViewer *viewer = (PDFKBasicPDFViewer *)segue.destinationViewController;
            viewer.enableBookmarks = YES;
            viewer.enableOpening = YES;
            viewer.enablePrinting = YES;
            viewer.enableSharing = YES;
            viewer.enableThumbnailSlider = YES;

            NSString *lyricsPath = [[NSUserDefaults standardUserDefaults]
                                    stringForKey:@"lyricsPath"];
            NSLog(@"lyricsPath==%@",lyricsPath);
            
            PDFKDocument *document = [PDFKDocument documentWithContentsOfFile:lyricsPath password:nil];
            NSLog(@"document==%@",document);

            [viewer loadDocument:document];

        }

        if ([[segue identifier] isEqualToString:@"showLyricsOffline"])
        {
            
            LyricsOfflineDetailVC *kvc = [segue destinationViewController];
            kvc.lyricsTitleStr=_lyricsTitle;
            kvc.lyricsInfoStr=_lyricsPath;
            kvc.indxpath=_indexp;
        }
    }



    @end
