//
//  LyricsOfflineTableViewCell.h
//  SanjayMittalBhajan
//
//  Created by PRIYA GONGAL on 27/09/1940 Saka.
//  Copyright © 1940 Eshiksa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LyricsOfflineTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lyricsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lyricspath;
@property (weak, nonatomic) IBOutlet UIImageView *pdfImg;
@property(nonatomic,retain)NSString *lyricsPathStr;

@end

NS_ASSUME_NONNULL_END
