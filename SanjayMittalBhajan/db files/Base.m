//
//  Base.m
//  SanjayMittalBhajan
//  Created by Punit on 31/07/18.
//  Copyright © 2018 Eshiksa. All rights reserved.

#import "Base.h"

@implementation Base

NSString * const mainUrl = @"http://35.200.153.165:8080/Sanjay_Mittal_Bhajans/apis/";
NSString * const imgUrl=@"http://35.200.153.165:8080/Sanjay_Mittal_Bhajans";
NSString * const splashScreenUrl=@"http://35.200.153.165:8080/Sanjay_Mittal_Bhajans/apis/splashScreen/";

NSString * const videoDownloadUrl=@"http://35.200.153.165:8080/Sanjay_Mittal_Bhajans/apis/video/download/";
NSString * const videoUrl=@"http://35.200.153.165:8080";

NSString * const login = @"user/login";
NSString * const registration = @"user/addUser";
NSString * const feedback = @"saveFeedback";
NSString * const gallery = @"getGalleryFolders/1";
NSString * const galleryfolder = @"getGalleryFolders/1/1";
NSString * const events = @"events/eventsList";
NSString * const audio = @"audio";
NSString * const getGalleryFolders = @"getGalleryFolders";
NSString * const galleryImage = @"galleryImage";
NSString * const pdfList = @"pdf/pdf-list?pageNo=%d";
NSString * const calender = @"Calender";
NSString * const upcomingEvent = @"events/upcomingEvent";
NSString * const videoList = @"video/video-list?pageNo=%d";
NSString * const audioList = @"audio/audio-list?pageNo=%d";
NSString * const splashScreen = @"apis/splashScreen/get";




@end
