
#import <UIKit/UIKit.h>
@interface AudioListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLSessionDelegate,UITextViewDelegate,UISearchBarDelegate,UISearchControllerDelegate>
{
    dispatch_queue_t queue;
    NSInteger indexBtn;
    NSData *data;
    NSString *filePath;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property (nonatomic,strong) NSMutableArray *audioListArr,*filteredArray;

@property(nonatomic,retain)NSString *status,*indexp,*message,*details,*currentPageNo,*totalCount,*totalPageSize,*fileIdStr,*filenameStr,*bhajanTitle,*audioPath;

@property(nonatomic,retain)UIImage *audioImg;
@property (nonatomic,strong) NSMutableArray *imgarray;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
-(void)dataAudioParsing;




@end
