//how to show local directory mp3 list in objective c
#import "AudioListViewController.h"
#import "Base.h"
#import "AudioTableViewCell.h"
#import "AudioPlayViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "DGActivityIndicatorView.h"
@interface AudioListViewController ()
{
    BOOL isFiltered;
    int indxp;
     NSUInteger previousPosition;
    int startPage;
    int lastPage ;
    NSString *myPath;
    NSURL *finalURL;
    NSString *fileurl;
     DGActivityIndicatorView *activityIndicatorView;
}
@end


@implementation AudioListViewController
@synthesize searchBar;
- (void)viewDidLoad {
    
    [super viewDidLoad];
    startPage = 1 ;
    lastPage = 1 ;
    [self.tableview setSeparatorColor:[UIColor clearColor]];
  
    searchBar.delegate = (id)self;
    _tableview.delegate=self;
    _tableview.dataSource=self;
    _audioListArr=[[NSMutableArray alloc]init];
    _imgarray=[[NSMutableArray alloc]init];
   
    CFTimeInterval startTime = CACurrentMediaTime();
   
    CFTimeInterval elapsedTime = CACurrentMediaTime() - startTime;
    NSLog(@"****elapsedTime:%f",elapsedTime);//0.000942
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"Alert!" message:@"Please check internet connection" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action=[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               
                               {
                               }];
        
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.tableview animated:NO];
        hud.mode = MBProgressHUDAnimationFade;
        hud.labelText = @"Loading...";
        
        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor]];
        CGFloat width = self.view.bounds.size.width / 6.0f;
        CGFloat height = self.view.bounds.size.height / 6.0f;
        activityIndicatorView.frame = CGRectMake(20,40,width,height);
        
        [self dataAudioParsing];
    }
    
    
}

-(void)viewDidUnload{
    [self setSearchBar:nil];
    [super viewDidUnload];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"****searchText:%@",searchText);
    
    if(searchText.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        isFiltered = true;
        _filteredArray=[[NSMutableArray alloc]init];
        
        for (AudioInfo *temp in _audioListArr)
        {
            NSRange nameRange = [[temp.tag  description] rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(nameRange.location != NSNotFound)
            {
                [_filteredArray addObject:temp];
            }
        }
    }
    
    [self.tableview reloadData];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.tableview resignFirstResponder];
}

-(void)dataAudioParsing{
    
    if (startPage<=lastPage) {
        NSLog(@"start page=== %d",startPage) ;
    
    queue=dispatch_queue_create("images", DISPATCH_QUEUE_CONCURRENT);
    queue=dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0);
    
//    [_audioListArr removeAllObjects];
//    [_imgarray removeAllObjects];
//
    NSString *username = @"priya0102";
    NSString *password = @"priya12345";
    
    NSString *unpw = [NSString stringWithFormat:@"%@:%@",username,password];
    NSData *updata = [unpw dataUsingEncoding:NSASCIIStringEncoding];
    
    NSString *base64str = [NSString stringWithFormat:@"Basic %@", [updata base64Encoding]];
    NSDictionary *headers = @{ @"content-type": @"json/application",
                               @"authorization": base64str };
        
    NSString *mainstr=[NSString stringWithFormat:@"http://35.200.153.165:8080/Sanjay_Mittal_Bhajans/apis/audio/audio-list?pageNo=%d",startPage];
        
   // NSString *mainstr=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:audioList]];
     NSLog(@"*****url string==%@",mainstr);
        
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:mainstr] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
        if (error) {
            NSLog(@"%@", error);
        }
        else {
            
            NSLog(@"Success: %@", data);
            
            NSError *err;
            
            NSArray *jsonArray  = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            NSLog(@"JSON DATA%@",jsonArray);
           
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:[NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil] options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"response data:%@",maindic);
            
            self.status=[maindic objectForKey:@"status"];
            self.message=[maindic objectForKey:@"message"];
            
            NSArray *detailArr=[maindic objectForKey:@"details"];
            NSDictionary *bodyDic=[maindic objectForKey:@"body"];
            
            NSLog(@"status==%@& message=%@ details==%@  bodyDic==%@",self.status,self.message,detailArr,bodyDic);
            
            self.currentPageNo=[bodyDic objectForKey:@"currentPageNo"];
            self.totalCount=[bodyDic objectForKey:@"totalCount"];
//            self.totalPageSize=[bodyDic objectForKey:@"totalPageSize"];
//            NSLog(@"totalCount==%@",self.totalCount);
            lastPage = [bodyDic[@"totalPageSize"] intValue];
            NSLog(@"totalcount==%@, totalpagesize==%d",self.totalCount,lastPage);
            
            NSArray *bodyArr=[bodyDic objectForKey:@"body"];
            
            
            if(bodyArr.count==0)
            {
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"Currently there is no audio data." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alertView dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
                
                
                [alertView addAction:ok];
                
                [self presentViewController:alertView animated:YES completion:nil];
                
            }
            else {
                
                for(NSDictionary *temp in bodyArr)
                {
                    AudioInfo *audio=[[AudioInfo alloc]init];
                    
                    audio.thumbnailPathStr=[[temp objectForKey:@"thumbnailsPath"]description];
                    audio.audioPathStr=[[temp objectForKey:@"audioPath"]description];
                    audio.fileName=[[temp objectForKey:@"fileName"]description];
                    audio.audioSize=[[temp objectForKey:@"audioSize"]description];
                    audio.pdfFileName=[[temp objectForKey:@"pdfFileName"]description];
                    audio.fileId=[[temp objectForKey:@"fileId"]description];
                    audio.tag=[[temp objectForKey:@"tags"]description];
                    audio.bhajanTitleStr=[[temp objectForKey:@"bhajanTitle"]description];
                    
                    
                    [self->_imgarray addObject:audio.thumbnailPathStr];
                    
                    [self->_audioListArr addObject:audio];
                    NSLog(@"_audioListArr ARRAYY%@",self->_audioListArr);
                }
            }

            NSLog(@"count: %lu",(unsigned long)self.audioListArr.count) ;
            [self.tableview reloadData];
            [MBProgressHUD hideHUDForView:self.tableview animated:NO];
        }
    });
    }];
    [dataTask resume];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger rowCount;
    if(isFiltered)
        rowCount = _filteredArray.count;
    else
        rowCount = _audioListArr.count;
    
    
    return rowCount;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AudioTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if(isFiltered){
        AudioInfo *ktemp=[_filteredArray objectAtIndex:indexPath.row];
        
        cell.audioTitle.text=ktemp.bhajanTitleStr;
        cell.thumbnailPath.text=ktemp.thumbnailPathStr;
        cell.audioPath.text=ktemp.audioPathStr;
        cell.audioSize.text=ktemp.audioSize;
        cell.tags.text=ktemp.tag;
        cell.fileId.text= ktemp.fileId;
        cell.fileName.text=ktemp.fileName;
        
        
        NSString *urlstr = [imgUrl stringByAppendingString:ktemp.thumbnailPathStr];
        
        NSLog(@"URL PATH=%@",urlstr);
        if ([urlstr containsString:@"<null>"]) {
            cell.audioImg.image=[UIImage imageNamed:@"Audioimage.png"];
        }
        else{
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                UIImage *img1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:     [NSURL URLWithString:urlstr]]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    cell.audioImg.image = img1;
                    
                });
            });
        }
        return cell;
    }
    else{
        AudioInfo *ktemp=[_audioListArr objectAtIndex:indexPath.row];
        
        cell.audioTitle.text=ktemp.bhajanTitleStr;
        cell.thumbnailPath.text=ktemp.thumbnailPathStr;
        cell.audioPath.text=ktemp.audioPathStr;
        cell.audioSize.text=ktemp.audioSize;
        cell.tags.text=ktemp.tag;
        cell.fileId.text= ktemp.fileId;
        cell.fileName.text=ktemp.fileName;
        
        cell.audioButton.tag=indexPath.row;
        [cell.audioButton addTarget:self action:@selector(audioButtonclicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        NSString *urlstr = [imgUrl stringByAppendingString:ktemp.thumbnailPathStr];
        
        NSLog(@"URL PATH=%@",urlstr);
        if ([urlstr containsString:@"<null>"]) {
            cell.audioImg.image=[UIImage imageNamed:@"Audioimage.png"];
        }
        else{
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                UIImage *img1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstr]]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    cell.audioImg.image = img1;
                    
                });
            });
        }
        if (indexPath.row == _audioListArr.count-1) {
            startPage = startPage + 1 ;
            [self dataAudioParsing] ;
        }
        return cell;
    }
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 106;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
 
    AudioTableViewCell *cell=[_tableview cellForRowAtIndexPath:indexPath];
    
    _fileIdStr=cell.fileId.text;
    _filenameStr=cell.fileName.text;
    _bhajanTitle=cell.audioTitle.text;
    _audioImg=cell.audioImg.image;
    _audioPath=cell.audioPath.text;
    indxp=indexPath.row;
    
    NSLog(@"audio indexpath==%ld",(long)indexPath.row);
    

    dispatch_async( dispatch_get_main_queue(),
                   ^{
                       [self performSegueWithIdentifier:@"audioListDetails"
                                                 sender:[self.tableview cellForRowAtIndexPath:indexPath]];
                   });

}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"audioListDetails"])
    {
        
        AudioPlayViewController *kvc = [segue destinationViewController];
        AudioInfo *maudio=[[AudioInfo alloc]init];
        
        maudio.fileName=_filenameStr;
        maudio.fileId=_fileIdStr;
        maudio.bhajanTitleStr=_bhajanTitle;
        maudio.audioPathStr=_audioPath;
        
        kvc.audioInfo = maudio;
        kvc.audioListArr = [[NSMutableArray alloc]init];
        kvc.audioListArr = isFiltered?_filteredArray:_audioListArr;
        kvc.selectedIndex = indxp;
        NSLog(@"indexpath in prepare for segue==%@ & audio image=%@ kvc.selectedIndex=%l",_fileIdStr,_audioImg,kvc.selectedIndex);
        
    }
}
-(void)audioButtonclicked:(UIButton*)sender{
    
    
    NSString *emailid=[[NSUserDefaults standardUserDefaults]stringForKey:@"username"];
    NSLog(@"email id==%@",emailid);
    
    UIButton *btn =(UIButton *)sender;
    NSLog(@"Btn Click...........%ld",(long)btn.tag);
    
    indexBtn=sender.tag;
    NSLog(@"INDEX video===%ld",indexBtn);
    
    if (emailid==NULL) {
        UIAlertController * alert=[UIAlertController
                                   
                                   alertControllerWithTitle:@"Sorry!" message:@"You can't download media because you are not login.For downloading you need to login first!!"preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        NSLog(@"you pressed ok, please button");
                                        
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
       [self saveAudioInDocumentsDirectory];
       [self showAlert:@"Alert" :@"File downloaded!"];
    }
    
}

-(NSData *)saveAudioInDocumentsDirectory
{
    BOOL success = NO;
    AudioInfo *ktemp=[_audioListArr objectAtIndex:indexBtn];
    
    NSString *audio=ktemp.audioPathStr;
    NSString *filename=ktemp.fileName;
    NSString *fileidStr=ktemp.fileId;
     NSString *fileTitle=ktemp.bhajanTitleStr;
    NSString *fileext=@".mp3";
    NSString *filetype=@"audio";
    
    NSString *str3=[NSString stringWithFormat:@"%@",[imgUrl stringByAppendingString:audio]];
    
    NSString *str1=[@"/" stringByAppendingFormat:@"%@",filename];
    NSLog(@"***str1***%@",str1);
    
    fileurl=[str3 stringByAppendingFormat:@"%@",str1];
    NSLog(@"***file***%@",fileurl);
    
    //NSURL *url= [[NSURL alloc]initWithString:fileurl];
    finalURL = [NSURL URLWithString:[fileurl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    NSLog(@"***finalURLString***%@",finalURL);
    
    data = [NSData dataWithContentsOfURL:finalURL];
    
  //  [[DBManager getSharedInstance]saveData:regNoTextField.text name:nameTextField.text department:departmentTextField.text year:yearTextField.text];

    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsPath = [paths objectAtIndex:[_indexp intValue]]; //Get the docs directory
   
    filePath = [documentsPath stringByAppendingPathComponent:[[fileurl componentsSeparatedByString:@"/"]lastObject]];
    NSLog(@"filePath %@",filePath);
    
    [data writeToFile:filePath atomically:YES];
    
   // success=[[DBManager getSharedInstance]saveData:fileidStr file_name:filename file_path:filePath file_extension:@".mp3" file_type:@"audio"];
    
   // NSFileManager *fileM=[NSFileManager defaultManager];
    //[fileM moveItemAtURL:location toURL:[NSURL fileURLWithPath:myPath] error:nil];
    
    printf("Video file == %s",[filePath UTF8String]);//2nd implemented
    UISaveVideoAtPathToSavedPhotosAlbum (filePath,self, @selector(video:didFinishSavingWithError: contextInfo:), nil);
    
    return data;
}
- (void) video: (NSString *) videoPath didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    NSLog(@"Finished saving video with error: %@", error);
    
}
-(void)fetchAudioInDocumentsDirectory:(NSString*)filepath
{
    data = [NSData dataWithContentsOfFile:filePath]; // fetch image data from filepath
    NSLog(@"dataWithContentsOfFile:%@",data);
    
}
- (IBAction)viewDownloadBtnClciked:(id)sender {
    
    NSLog(@"fetchImageInDocumentsDirectory:");
    [self fetchAudioInDocumentsDirectory:filePath];
    
}
-(void)showAlert:(NSString *)title :(NSString*)message{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end

//#import "AudioListViewController.h"
//#import "Base.h"
//#import "AudioTableViewCell.h"
//#import "AudioPlayViewController.h"
//@interface AudioListViewController ()
//{
//        BOOL isFiltered;
//}
//@end
//
//
//@implementation AudioListViewController
//@synthesize searchBar;
//- (void)viewDidLoad {
//
//    [super viewDidLoad];
//    [self.tableview setSeparatorColor:[UIColor clearColor]];
//
//     searchBar.delegate = (id)self;
//    _tableview.delegate=self;
//    _tableview.dataSource=self;
//    _audioListArr=[[NSMutableArray alloc]init];
//    _imgarray=[[NSMutableArray alloc]init];
//    self.indicator.hidden=YES;
//    [self dataAudioParsing];
//}
//
//-(void)viewDidUnload{
// [self setSearchBar:nil];
// [super viewDidUnload];
// }
// - (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
//    NSLog(@"****searchText:%@",searchText);
//
//    if(searchText.length == 0)
//    {
//        isFiltered = FALSE;
//    }
//    else
//    {
//        isFiltered = true;
//        _filteredArray=[[NSMutableArray alloc]init];
//
//        for (NSDictionary *temp in _audioListArr)
//        {
//            NSRange nameRange = [[[temp objectForKey:@"tags"]description] rangeOfString:searchText options:NSCaseInsensitiveSearch];
//
//            if(nameRange.location != NSNotFound)
//            {
//                [_filteredArray addObject:temp];
//            }
//        }
//    }
//
//    [self.tableview reloadData];
//}
//
//
//- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
//    [self.tableview resignFirstResponder];
//}
//
//-(void)dataAudioParsing{
//    queue=dispatch_queue_create("images", DISPATCH_QUEUE_CONCURRENT);
//    queue=dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0);
//
//        [_audioListArr removeAllObjects];
//        [_imgarray removeAllObjects];
//
//        NSString *username = @"priya0102";
//        NSString *password = @"priya12345";
//
//        NSString *unpw = [NSString stringWithFormat:@"%@:%@",username,password];
//        NSData *updata = [unpw dataUsingEncoding:NSASCIIStringEncoding];
//
//        NSString *base64str = [NSString stringWithFormat:@"Basic %@", [updata base64Encoding]];
//        NSDictionary *headers = @{ @"content-type": @"json/application",
//                                   @"authorization": base64str };
//        NSString *mainstr=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:audioList]];
//
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:mainstr] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
//        [request setHTTPMethod:@"POST"];
//        [request setAllHTTPHeaderFields:headers];
//        NSURLSession *session = [NSURLSession sharedSession];
//        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//            if (error) {
//                NSLog(@"%@", error);
//            }
//            else {
//
//                NSLog(@"Success: %@", data);
//
//                NSError *err;
//
//                NSArray *jsonArray  = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
//                NSLog(@"JSON DATA%@",jsonArray);
//
//                NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:[NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil] options:NSJSONReadingMutableContainers error:nil];
//
//               // NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil]; hi gh
//
//                NSLog(@"response data:%@",maindic);
//
//                self.status=[maindic objectForKey:@"status"];
//                self.message=[maindic objectForKey:@"message"];
//
//                NSArray *detailArr=[maindic objectForKey:@"details"];
//                NSDictionary *bodyDic=[maindic objectForKey:@"body"];
//
//                NSLog(@"status==%@& message=%@ details==%@  bodyDic==%@",self.status,self.message,detailArr,bodyDic);
//
//                self.currentPageNo=[bodyDic objectForKey:@"currentPageNo"];
//                self.totalCount=[bodyDic objectForKey:@"totalCount"];
//                 self.totalPageSize=[bodyDic objectForKey:@"totalPageSize"];
//                NSLog(@"totalCount==%@",self.totalCount);
//
//
//                 NSArray *bodyArr=[bodyDic objectForKey:@"body"];
//
//
//                if(bodyArr.count==0)
//                {
//                    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"Currently there is no audio data." preferredStyle:UIAlertControllerStyleAlert];
//
//                    UIAlertAction* ok = [UIAlertAction
//                                         actionWithTitle:@"OK"
//                                         style:UIAlertActionStyleDefault
//                                         handler:^(UIAlertAction * action)
//                                         {
//                                             [alertView dismissViewControllerAnimated:YES completion:nil];
//
//                                         }];
//
//
//                    [alertView addAction:ok];
//
//                    [self presentViewController:alertView animated:YES completion:nil];
//
//                }
//                else {
//
//                    for(NSDictionary *temp in bodyArr)
//                    {
//                        NSString *str1=[[temp objectForKey:@"thumbnailsPath"]description];
//                        NSString *str2=[[temp objectForKey:@"audioPath"]description];
//                        NSString *str3=[[temp objectForKey:@"fileName"]description];
//                        NSString *str4=[[temp objectForKey:@"audioSize"]description];
//                        NSString *str5=[[temp objectForKey:@"pdfFileName"]description];
//                        NSString *str6=[[temp objectForKey:@"fileId"]description];
//                        NSString *str7=[[temp objectForKey:@"tags"]description];
//                        NSString *str8=[[temp objectForKey:@"bhajanTitle"]description];
//
//                        NSLog(@"date=%@  day=%@ venue=%@ organisation=%@  contactPerson=%@  contactNumber=%@ imagePath=%@ eventCount=%@",str1,str2,str3,str4,str5,str6,str7,str8);
//                        [self->_imgarray addObject:str1];
//
//                        [self->_audioListArr addObject:temp];
//                        NSLog(@"_audioListArr ARRAYY%@",self->_audioListArr);
//                    }
//                }
//                [self->_tableview performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
//
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [self.tableview reloadData];
//                });
//
//            }
//        }];
//        [dataTask resume];
//
//
//}
//
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    NSUInteger rowCount;
//    if(isFiltered)
//        rowCount = _filteredArray.count;
//    else
//        rowCount = _audioListArr.count;
//
//
//    return rowCount;
//
//}
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    AudioTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
//  if(isFiltered){
//    NSMutableDictionary *ktemp=[_filteredArray objectAtIndex:indexPath.row];
//
//    cell.audioTitle.text=[[ktemp objectForKey:@"bhajanTitle"]description];
//    cell.thumbnailPath.text=[[ktemp objectForKey:@"thumbnailsPath"]description];
//    cell.audioPath.text=[[ktemp objectForKey:@"audioPath"]description];
//    cell.audioSize.text=[[ktemp objectForKey:@"audioSize"]description];
//    cell.tags.text=[[ktemp objectForKey:@"tags"]description];
//    cell.fileId.text= [[ktemp objectForKey:@"fileId"]description];
//    cell.fileName.text=[[ktemp objectForKey:@"fileName"]description];
//
//
//    NSString *urlstr = [imgUrl stringByAppendingString:[[ktemp objectForKey:@"thumbnailsPath"]description]];
//
//    NSLog(@"URL PATH=%@",urlstr);
//      if ([urlstr containsString:@"<null>"]) {
//          cell.audioImg.image=[UIImage imageNamed:@"Audioimage.png"];
//      }
//      else{
//          dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        UIImage *img1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:     [NSURL URLWithString:urlstr]]];
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//
//            cell.audioImg.image = img1;
//
//        });
//     });
//      }
//        return cell;
//  }
//  else{
//      NSMutableDictionary *ktemp=[_audioListArr objectAtIndex:indexPath.row];
//
//      cell.audioTitle.text=[[ktemp objectForKey:@"bhajanTitle"]description];
//      cell.thumbnailPath.text=[[ktemp objectForKey:@"thumbnailsPath"]description];
//      cell.audioPath.text=[[ktemp objectForKey:@"audioPath"]description];
//      cell.audioSize.text=[[ktemp objectForKey:@"audioSize"]description];
//      cell.tags.text=[[ktemp objectForKey:@"tags"]description];
//      cell.fileId.text= [[ktemp objectForKey:@"fileId"]description];
//      cell.fileName.text=[[ktemp objectForKey:@"fileName"]description];
//      cell.audioButton.tag=indexPath.row;
//      [cell.audioButton addTarget:self action:@selector(audioButtonclicked:) forControlEvents:UIControlEventTouchUpInside];
//
//
//      NSString *urlstr = [imgUrl stringByAppendingString:[[ktemp objectForKey:@"thumbnailsPath"]description]];
//
//      NSLog(@"URL PATH=%@",urlstr);
//      if ([urlstr containsString:@"<null>"]) {
//          cell.audioImg.image=[UIImage imageNamed:@"Audioimage.png"];
//      }
//      else{
//          dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//          UIImage *img1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstr]]];
//
//          dispatch_async(dispatch_get_main_queue(), ^{
//
//              cell.audioImg.image = img1;
//
//          });
//      });
//      }
//      return cell;
//  }
//    return cell;
//
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 106;
//}
//
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
//{
//    AudioTableViewCell *cell=[_tableview cellForRowAtIndexPath:indexPath];
//
//    _fileIdStr=cell.fileId.text;
//    _filenameStr=cell.fileName.text;
//    _bhajanTitle=cell.audioTitle.text;
//    _audioImg=cell.audioImg.image;
//    _audioPath=cell.audioPath.text;
//    _indxp=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
//
//    NSLog(@"indexpath==%ld",(long)indexPath.row);
//
//    [self performSegueWithIdentifier:@"audioListDetails"
//                              sender:[self.tableview cellForRowAtIndexPath:indexPath]];
//
//}
//
//
//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//
//    if ([[segue identifier] isEqualToString:@"audioListDetails"])
//    {
//
//        AudioPlayViewController *kvc = [segue destinationViewController];
//
//        kvc.fileNameStr=_filenameStr;
//        kvc.fileIdStr=_fileIdStr;
//        kvc.bhajanTitleStr=_bhajanTitle;
//        kvc.indxpath=_indxp;
//        kvc.kimg=_audioImg;
//        kvc.kimgarray=_imgarray;
//        kvc.audioPathStr=_audioPath;
//
//        NSLog(@"indexpath in prepare for segue==%@ & audio image=%@",_fileIdStr,_audioImg);
//    }
//}
//-(void)audioButtonclicked:(id)sender{
//
//    NSString *emailid=[[NSUserDefaults standardUserDefaults]stringForKey:@"username"];
//    NSLog(@"email id==%@",emailid);
//
//    UIButton *btn =(UIButton *)sender;
//    NSLog(@"Btn Click...........%ld",(long)btn.tag);
//
//    if (emailid==NULL) {
//        UIAlertController * alert=[UIAlertController
//
//                                   alertControllerWithTitle:@"Sorry!" message:@"You can't download media because you are not login.For downloading you need to login first!!"preferredStyle:UIAlertControllerStyleAlert];
//
//        UIAlertAction* yesButton = [UIAlertAction
//                                    actionWithTitle:@"OK"
//                                    style:UIAlertActionStyleDefault
//                                    handler:^(UIAlertAction * action)
//                                    {
//                                        NSLog(@"you pressed ok, please button");
//
//                                    }];
//
//        [alert addAction:yesButton];
//
//        [self presentViewController:alert animated:YES completion:nil];
//    }
//    else {
//
//        NSMutableDictionary *ktemp=[_audioListArr objectAtIndex:(long)btn.tag];
//
//           NSString *audio=[[ktemp objectForKey:@"audioPath"]description];
//           NSString *filename=[[ktemp objectForKey:@"fileName"]description];
//
//         NSString *str3=[NSString stringWithFormat:@"%@",[imgUrl stringByAppendingString:audio]];
//
//        NSString *str1=[@"/" stringByAppendingFormat:@"%@",filename];
//        NSLog(@"***str1***%@",str1);
//
//        NSString *fileurl=[str3 stringByAppendingFormat:@"%@",str1];
//        NSLog(@"***file***%@",fileurl);
//
//    NSURL *finalURLString = [NSURL URLWithString:[fileurl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
//
//        //NSURL *url = [NSURL URLWithString:fileurl];
//        NSLog(@"download url:::%@",finalURLString);
//        [self.indicator startAnimating];
//
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//            NSLog(@"Downloading Started");
//
//            NSData *data = [NSData dataWithContentsOfURL:finalURLString];
//
//            if(data){
//                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//
//                NSString *documentsDirectory = [paths objectAtIndex:0];
//
//                NSString *pdfPath = [documentsDirectory stringByAppendingPathComponent:@"Documents/audio.mp3"];
//
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [data writeToFile:pdfPath atomically:YES];
//                    NSLog(@"File Saved !");
//                    [self.indicator stopAnimating];
//                    self.indicator.hidden=YES;
//                    [self showAlert:@"Alert" :@"Filed saved!"];
//                });
//            }
//        });
//    }
//
//}
//-(void)showAlert:(NSString *)title :(NSString*)message{
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
//                                                    message:message
//                                                   delegate:self
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//    [alert show];
//}
//
//
//@end

/*
 //success = [[DBManager getSharedInstance]saveData:regNoTextField.text name:nameTextField.text department:departmentTextField.text  AudioInfo *ktemp=[_audioListArr objectAtIndex:indexBtn];
 
 
 //  NSMutableDictionary *ktemp=[_audioListArr objectAtIndex:(long)btn.tag];
 AudioInfo *ktemp=[_audioListArr objectAtIndex:(long)btn.tag];
 
 NSString *audio=[[ktemp objectForKey:@"audioPath"]description];
 NSString *filename=[[ktemp objectForKey:@"fileName"]description];
 
 NSString *str3=[NSString stringWithFormat:@"%@",[imgUrl stringByAppendingString:audio]];
 
 NSString *str1=[@"/" stringByAppendingFormat:@"%@",filename];
 NSLog(@"***str1***%@",str1);
 
 NSString *fileurl=[str3 stringByAppendingFormat:@"%@",str1];
 NSLog(@"***file***%@",fileurl);
 
 NSURL *finalURLString = [NSURL URLWithString:[fileurl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
 
 //NSURL *url = [NSURL URLWithString:fileurl];
 NSLog(@"download url:::%@",finalURLString);
 [self.indicator startAnimating];
 
 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
 NSLog(@"Downloading Started");
 
 NSData *data = [NSData dataWithContentsOfURL:finalURLString];
 
 if(data){
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 
 NSString *documentsDirectory = [paths objectAtIndex:0];
 
 NSString *pdfPath = [documentsDirectory stringByAppendingPathComponent:@"filename.mp3"];
 
 dispatch_async(dispatch_get_main_queue(), ^{
 [data writeToFile:pdfPath atomically:YES];
 NSLog(@"File Saved !");
 [self.indicator stopAnimating];
 self.indicator.hidden=YES;
 [self showAlert:@"Alert" :@"Filed saved!"];
 });
 }
 });*/

