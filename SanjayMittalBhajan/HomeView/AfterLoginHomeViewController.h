

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AfterLoginHomeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (weak, nonatomic) IBOutlet UIView *contentview;
@property (weak, nonatomic) IBOutlet UIView *homeView;

@property (weak, nonatomic) IBOutlet UIView *offlineView;

@property(nonatomic,retain)NSString *indxp,*status,*message,*details;
@property (nonatomic,strong) NSMutableArray *splashArr;
@property (weak, nonatomic) IBOutlet UIButton *shareBtnClicked;
- (IBAction)shareButtonClicked:(id)sender;


@end

NS_ASSUME_NONNULL_END
