

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController<UIScrollViewDelegate>


@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (weak, nonatomic) IBOutlet UIView *contentview;
@property (weak, nonatomic) IBOutlet UIView *homeView;
- (IBAction)shareBtnClicked:(id)sender;

@property(nonatomic,retain)NSString *indxp,*status,*message,*details;
@property (nonatomic,strong) NSMutableArray *splashArr;
@end
