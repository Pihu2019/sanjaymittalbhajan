
#import <UIKit/UIKit.h>

@interface CalendarViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLSessionDelegate,UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property(nonatomic,retain)NSString *indxp,*status,*message,*details;
@property (nonatomic,strong) NSMutableArray *calendarArr;
@end
