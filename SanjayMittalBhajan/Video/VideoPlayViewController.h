

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoPlayViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *videopath;
@property (weak, nonatomic) IBOutlet UILabel *videolbl;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property(nonatomic,retain)NSString *indxp,*videoPath,*videolabel;
@end

NS_ASSUME_NONNULL_END
