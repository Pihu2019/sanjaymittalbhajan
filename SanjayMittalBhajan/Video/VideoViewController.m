
//https://www.techotopia.com/index.php/Video_Playback_from_within_an_iOS_7_Application

#import "VideoViewController.h"
#import "VideoTableViewCell.h"
#import "Base.h"
#import "VideoPlayViewController.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "DGActivityIndicatorView.h"
@interface VideoViewController ()
{
    BOOL isFiltered;
    int startPage;
    int lastPage ;
    NSString*videoPathStr;
    DGActivityIndicatorView *activityIndicatorView;
}
@end

@implementation VideoViewController
@synthesize searchBar;
- (void)viewDidLoad {
    [super viewDidLoad];
    startPage = 1 ;
    lastPage = 1 ;
    [self.tableview setSeparatorColor:[UIColor clearColor]];
    
    searchBar.delegate=(id)self;
    _tableview.delegate=self;
    _tableview.dataSource=self;
    _videoListArr=[[NSMutableArray alloc]init];
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"Alert!" message:@"Please check internet connection" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action=[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               
                               {
                               }];
        
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.tableview animated:NO];
        hud.mode = MBProgressHUDAnimationFade;
        hud.labelText = @"Loading...";
        
        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor]];
        CGFloat width = self.view.bounds.size.width / 6.0f;
        CGFloat height = self.view.bounds.size.height / 6.0f;
        activityIndicatorView.frame = CGRectMake(20,40,width,height);
        
         [self dataVideoParsing];
        
        
    }
  
}
-(void)viewDidUnload{
    [self setSearchBar:nil];
    [super viewDidUnload];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"****searchText:%@",searchText);
    
    if(searchText.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        isFiltered = true;
         _filteredArray=[[NSMutableArray alloc]init];
        for (NSDictionary *temp in _videoListArr)
        {
            NSRange nameRange = [[[temp objectForKey:@"tags"]description] rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(nameRange.location != NSNotFound)
            {
                [_filteredArray addObject:temp];
            }
        }
    }
    
    [self.tableview reloadData];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.tableview resignFirstResponder];
}
-(void)dataVideoParsing{
    if (startPage<=lastPage) {
        NSLog(@"start page=== %d",startPage) ;
        
  
    NSString *username = @"priya0102";
    NSString *password = @"priya12345";
    
    NSString *unpw = [NSString stringWithFormat:@"%@:%@",username,password];
    NSData *updata = [unpw dataUsingEncoding:NSASCIIStringEncoding];
    
    NSString *base64str = [NSString stringWithFormat:@"Basic %@", [updata base64Encoding]];
    NSDictionary *headers = @{ @"content-type": @"json/application",
                               @"authorization": base64str };

        NSString *mainstr=[NSString stringWithFormat:@"http://35.200.153.165:8080/Sanjay_Mittal_Bhajans/apis/video/video-list?pageNo=%d",startPage];
        
            NSLog(@"*****url string==%@",mainstr);
        
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:mainstr] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
         dispatch_async(dispatch_get_main_queue(), ^{
        if (error) {
            NSLog(@"%@", error);
        }
        else {
            
            NSLog(@"Success: %@", data);
            
            NSError *err;
            
            NSArray *jsonArray  = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            NSLog(@"JSON DATA%@",jsonArray);
            
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:[NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil] options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"response data:%@",maindic);
            
            self.status=[maindic objectForKey:@"status"];
            self.message=[maindic objectForKey:@"message"];
            
            NSArray *detailArr=[maindic objectForKey:@"details"];
            NSDictionary *bodyDic=[maindic objectForKey:@"body"];
            
            NSLog(@"status==%@& message=%@ details==%@  bodyDic==%@",self.status,self.message,detailArr,bodyDic);
            
            self.currentPageNo=[bodyDic objectForKey:@"currentPageNo"];
            self.totalCount=[bodyDic objectForKey:@"totalCount"];
//            self.totalPageSize=[bodyDic objectForKey:@"totalPageSize"];
//            NSLog(@"status==%@",self.totalCount);
            lastPage = [bodyDic[@"totalPageSize"] intValue];
            NSLog(@"totalcount==%@, totalpagesize==%d",self.totalCount,lastPage);
            NSArray *bodyArr=[bodyDic objectForKey:@"body"];
            
            
            if(bodyArr.count==0)
            {
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"Currently there is no video data." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alertView dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
                
                
                [alertView addAction:ok];
                
                [self presentViewController:alertView animated:YES completion:nil];
                
            }
            else {
                
                for(NSDictionary *temp in bodyArr)
                {
                    NSString *str1=[[temp objectForKey:@"thumbnailsPath"]description];
                    NSString *str2=[[temp objectForKey:@"videoPath"]description];
                    NSString *str3=[[temp objectForKey:@"fileName"]description];
                    NSString *str4=[[temp objectForKey:@"videoSize"]description];
                    NSString *str5=[[temp objectForKey:@"tags"]description];
                    NSString *str6=[[temp objectForKey:@"bhajanTitle"]description];
                     NSString *str7=[[temp objectForKey:@"fileId"]description];
                    
                    NSLog(@"date=%@  day=%@ venue=%@ organisation=%@  contactPerson=%@  contactNumber=%@fileId=%@",str1,str2,str3,str4,str5,str6,str7);
                    
                    
                    [self->_videoListArr addObject:temp];
                    NSLog(@"_videoListArr ARRAYY%@",self->_videoListArr);
                }
            }

            NSLog(@"count: %lu",(unsigned long)self.videoListArr.count) ;
            [self.tableview reloadData];
            [MBProgressHUD hideHUDForView:self.tableview animated:NO];
        }
        });
    }];
    [dataTask resume];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger rowCount;
    if (isFiltered)
        rowCount=_filteredArray.count;
    else
        rowCount=_videoListArr.count;
    return  rowCount;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VideoTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if (isFiltered) {
        NSMutableDictionary *ktemp=[_filteredArray objectAtIndex:indexPath.row];
        
        cell.videoLbl.text=[[ktemp objectForKey:@"bhajanTitle"]description];
        cell.thumbnailPath.text=[[ktemp objectForKey:@"thumbnailsPath"]description];
        cell.videoPath.text=[[ktemp objectForKey:@"videoPath"]description];
        cell.videoSize.text=[[ktemp objectForKey:@"videoSize"]description];
        cell.tags.text=[[ktemp objectForKey:@"tags"]description];
        cell.filename.text=[[ktemp objectForKey:@"fileName"]description];
        cell.fileId.text=[[ktemp objectForKey:@"fileId"]description];
        
        NSString *urlstr = [imgUrl stringByAppendingString:[[ktemp objectForKey:@"thumbnailsPath"]description]];
        NSLog(@"URL PATH=%@",urlstr);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *img1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:     [NSURL URLWithString:urlstr]]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                cell.imgVideo.image = img1;
                
            });
        });
    
        return cell;
    }
    else{
    NSMutableDictionary *ktemp=[_videoListArr objectAtIndex:indexPath.row];
    
    cell.videoLbl.text=[[ktemp objectForKey:@"bhajanTitle"]description];
    cell.thumbnailPath.text=[[ktemp objectForKey:@"thumbnailsPath"]description];
    cell.videoPath.text=[[ktemp objectForKey:@"videoPath"]description];
    cell.videoSize.text=[[ktemp objectForKey:@"videoSize"]description];
    cell.tags.text=[[ktemp objectForKey:@"tags"]description];
    cell.filename.text=[[ktemp objectForKey:@"fileName"]description];
    cell.fileId.text=[[ktemp objectForKey:@"fileId"]description];
        
    NSLog(@"videoPath PATH=%@  fileID =%@ videoPath2 PATH=%@ ",cell.videoPath.text,cell.fileId.text,[[ktemp objectForKey:@"videoPath"]description]);
        
    videoPathStr=[[ktemp objectForKey:@"videoPath"]description];
        
    cell.videoBtn.tag=indexPath.row;
    [cell.videoBtn addTarget:self action:@selector(videoDownloadBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
    NSString *urlstr = [imgUrl stringByAppendingString:[[ktemp objectForKey:@"thumbnailsPath"]description]];
    NSLog(@"URL PATH=%@",urlstr);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *img1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:     [NSURL URLWithString:urlstr]]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            cell.imgVideo.image = img1;
            
        });
    });
        if (indexPath.row == _videoListArr.count-1) {
            startPage = startPage + 1 ;
            [self dataVideoParsing] ;
        }
        return cell;
    }
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 157;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    VideoTableViewCell *cell=[_tableview cellForRowAtIndexPath:indexPath];
    
    _videolbl=cell.videoLbl.text;
    
    _videoPath=videoPathStr;

   
    _indxp=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
    
    NSLog(@"indexpath==%ld videopath=%@ videolabel=%@",(long)indexPath.row,_videoPath,_videolbl);
    
    [self performSegueWithIdentifier:@"videoPlay"
                              sender:[self.tableview cellForRowAtIndexPath:indexPath]];
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"videoPlay"])
    {
        
        VideoPlayViewController *kvc = [segue destinationViewController];
        
        kvc.videolabel=_videolbl;
        kvc.videoPath=_videoPath;
        kvc.indxp=_indxp;
      
        NSLog(@"indexpath in prepare for segue==%@ & audio image=%@",_videolbl,_videoPath);
    }
}
-(void)videoDownloadBtn:(UIButton*)sender{
    
    NSString *emailid=[[NSUserDefaults standardUserDefaults]stringForKey:@"username"];
    NSLog(@"email id==%@",emailid);
    
    UIButton *btn =(UIButton *)sender;
    NSLog(@"Btn Click...........%ld",(long)btn.tag);
    
    indexBtn=sender.tag;
    NSLog(@"INDEX video===%ld",indexBtn);
    if (emailid==NULL) {
        UIAlertController * alert=[UIAlertController
                                   
                                   alertControllerWithTitle:@"Sorry!" message:@"You can't download media because you are not login.For downloading you need to login first!!"preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        NSLog(@"you pressed ok, please button");
                                        
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        [self saveVideoInDocumentsDirectory];
       //[self showAlert:@"Alert" :@"File downloaded!"];
       
    }
    
}
-(NSData *)saveVideoInDocumentsDirectory
{


    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         NSMutableDictionary *ktemp=[_videoListArr objectAtIndex:indexBtn];
        NSString *fileid=[[ktemp objectForKey:@"fileId"]description];
        NSString *filename=[[ktemp objectForKey:@"fileName"]description];
        
        NSString *file=[NSString stringWithFormat:@"%@",[videoDownloadUrl stringByAppendingString:fileid]];
        
        
        NSLog(@"Downloading Started");
        NSString *urlToDownload =file;
        NSURL  *url = [NSURL URLWithString:urlToDownload];
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        if ( urlData )
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
          //  NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"thefile.mp4"];
            NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,filename];
            //saving is done on main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                [urlData writeToFile:filePath atomically:YES];
                NSLog(@"File Saved !");
                [self showAlert:@"Alert" :@"File downloaded!"];
                printf("Video file == %s",[filePath UTF8String]);//2nd implemented
                UISaveVideoAtPathToSavedPhotosAlbum (filePath,self, @selector(video:didFinishSavingWithError: contextInfo:), nil);
            });
        }
        
    });
    
    return data;
}
- (void) video: (NSString *) videoPath didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    NSLog(@"Finished saving video with error: %@", error);
    
}
-(void)fetchAudioInDocumentsDirectory:(NSString*)filepath
{
    data = [NSData dataWithContentsOfFile:filePath]; // fetch image data from filepath
    NSLog(@"dataWithContentsOfFile:%@",data);
    
}
- (IBAction)viewDownloadBtnClciked:(id)sender {
    
    NSLog(@"fetchImageInDocumentsDirectory:");
    [self fetchAudioInDocumentsDirectory:filePath];
    
}
-(void)showAlert:(NSString *)title :(NSString*)message{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
