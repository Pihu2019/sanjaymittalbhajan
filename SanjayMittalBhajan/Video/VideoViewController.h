

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface VideoViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLSessionDelegate,UITextViewDelegate,UISearchBarDelegate,UISearchControllerDelegate>
{
    NSInteger indexBtn;
    NSData *data;
    NSString *filePath;
}
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic,strong) NSMutableArray *videoListArr,*filteredArray;

@property(nonatomic,retain)NSString *indxp,*status,*message,*details,*currentPageNo,*totalCount,*totalPageSize,*videoPath,*videolbl,*indexp,*filename;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *videoView;

@end
