
#import "FeedbackViewController.h"
#import "Base.h"
#import "Constant.h"

@interface FeedbackViewController ()

@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imageViewReg.layer.cornerRadius = self.imageViewReg.frame.size.width / 2;
    self.imageViewReg.clipsToBounds = YES;
    
    [_email becomeFirstResponder];
    
    _email.delegate=self;
    _name.delegate=self;
    _contactNum.delegate=self;
    _feedbackText.delegate=self;
    
    UIColor *color = [UIColor grayColor];
    _email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: color}];
    _name.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name" attributes:@{NSForegroundColorAttributeName: color}];
    _contactNum.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Contact No." attributes:@{NSForegroundColorAttributeName: color}];
    _feedbackText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Input text here" attributes:@{NSForegroundColorAttributeName: color}];
    
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_feedbackView.bounds
                                                   byRoundingCorners:(UIRectCornerBottomRight | UIRectCornerBottomLeft)
                                                         cornerRadii:CGSizeMake(8.0, 8.0)];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = _feedbackView.bounds;
    maskLayer.path = maskPath.CGPath;
    _feedbackView.layer.mask = maskLayer;
}
- (IBAction)sendFeedBackBtnClicked:(id)sender {
    [self requestFeedbackdata];
}
-(void)requestFeedbackdata{
    
    NSDictionary *data = @{ @"email":self.email.text ,
                            @"name":self.name.text ,
                            @"contactNo":self.contactNum.text ,
                            @"feedbackText": self.feedbackText.text
                            };
    NSLog(@"dataaaa%@",data);
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:kNilOptions error:&error];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:feedback]]];
    
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url cachePolicy:nil timeoutInterval:60];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
    [req setHTTPMethod:@"POST"];
    [req setHTTPBody:jsonData];
    
    NSString *retStr = [[NSString alloc] initWithData:[NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil] encoding:NSUTF8StringEncoding];
    
    NSLog(@"Return resistration String%@",retStr);
    
    NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:[NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil] options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"response data:%@",maindic);
    
    self.status=[maindic objectForKey:@"status"];
    self.message=[maindic objectForKey:@"message"];
    
    NSArray *detailArr=[maindic objectForKey:@"details"];
    
    NSLog(@"status==%@& message=%@ details==%@",self.status,self.message,detailArr);
    
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        
    }];
    if ([self.message isEqual:@"SUCCESS"]) {
        
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success" message:@"Feedback submitted successfully" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [alertView dismissViewControllerAnimated:YES completion:nil];
                                 [self viewWillAppear:YES];
                                 self.email.text=@"";
                                 self.name.text=@"";
                                 self.contactNum.text=@"";
                                 self.feedbackText.text=@"";
                             }];
        
        [alertView addAction:ok];
        [self presentViewController:alertView animated:YES completion:nil];
        
        
    }
    else{
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:@"Entered credential is incorrect" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertView dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        [alertView addAction:ok];
        [self presentViewController:alertView animated:YES completion:nil];
    }
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)email:(id)sender {
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.email resignFirstResponder];
    [self.name resignFirstResponder];
    [self.contactNum resignFirstResponder];
    [self.feedbackText resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==self.email)
    {
        [textField resignFirstResponder];
        [self.name becomeFirstResponder];
    }
    else if(textField==self.name)
    {
        [textField resignFirstResponder];
        [self.contactNum becomeFirstResponder];
    }
    else if(textField==self.contactNum)
    {
        [textField resignFirstResponder];
        [self.feedbackText becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return  YES;
}
@end
