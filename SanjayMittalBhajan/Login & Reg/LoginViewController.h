

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;


@property (weak, nonatomic) IBOutlet UITextField *textFieldUsername;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;


@property (weak, nonatomic) IBOutlet UIImageView *imageViewLogin;
@property (nonatomic,retain) NSString *status,*success,*message,*details;
@property (weak, nonatomic) IBOutlet UIView *loginView;

@end
